'use babel';

import DropHungarian from '../lib/drop-hungarian';

// Use the command `window:run-package-specs` (cmd-alt-ctrl-p) to run specs.
//
// To run a specific `it` or `describe` block add an `f` to the front (e.g. `fit`
// or `fdescribe`). Remove the `f` to unfocus the block.

describe('DropHungarian', () => {
  let workspaceElement, activationPromise;

  beforeEach(() => {
    workspaceElement = atom.views.getView(atom.workspace);
    activationPromise = atom.packages.activatePackage('drop-hungarian');
  });

  describe('when the drop-hungarian:toggle event is triggered', () => {
    it('hides and shows the modal panel', () => {
      // Before the activation event the view is not on the DOM, and no panel
      // has been created
      expect(workspaceElement.querySelector('.drop-hungarian')).not.toExist();

      // This is an activation event, triggering it will cause the package to be
      // activated.
      atom.commands.dispatch(workspaceElement, 'drop-hungarian:toggle');

      waitsForPromise(() => {
        return activationPromise;
      });

      runs(() => {
        expect(workspaceElement.querySelector('.drop-hungarian')).toExist();

        let dropHungarianElement = workspaceElement.querySelector('.drop-hungarian');
        expect(dropHungarianElement).toExist();

        let dropHungarianPanel = atom.workspace.panelForItem(dropHungarianElement);
        expect(dropHungarianPanel.isVisible()).toBe(true);
        atom.commands.dispatch(workspaceElement, 'drop-hungarian:toggle');
        expect(dropHungarianPanel.isVisible()).toBe(false);
      });
    });

    it('hides and shows the view', () => {
      // This test shows you an integration test testing at the view level.

      // Attaching the workspaceElement to the DOM is required to allow the
      // `toBeVisible()` matchers to work. Anything testing visibility or focus
      // requires that the workspaceElement is on the DOM. Tests that attach the
      // workspaceElement to the DOM are generally slower than those off DOM.
      jasmine.attachToDOM(workspaceElement);

      expect(workspaceElement.querySelector('.drop-hungarian')).not.toExist();

      // This is an activation event, triggering it causes the package to be
      // activated.
      atom.commands.dispatch(workspaceElement, 'drop-hungarian:toggle');

      waitsForPromise(() => {
        return activationPromise;
      });

      runs(() => {
        // Now we can test for view visibility
        let dropHungarianElement = workspaceElement.querySelector('.drop-hungarian');
        expect(dropHungarianElement).toBeVisible();
        atom.commands.dispatch(workspaceElement, 'drop-hungarian:toggle');
        expect(dropHungarianElement).not.toBeVisible();
      });
    });
  });
});
