'use babel';

import DropHungarianView from './drop-hungarian-view';
import { CompositeDisposable } from 'atom';

export default {

    dropHungarianView: null,
    modalPanel: null,
    subscriptions: null,

    activate(state) {
        this.dropHungarianView = new DropHungarianView(state.dropHungarianViewState);
        this.modalPanel = atom.workspace.addModalPanel({
            item: this.dropHungarianView.getElement(),
            visible: false
        });

        // Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
        this.subscriptions = new CompositeDisposable();

        // Register command that toggles this view
        this.subscriptions.add(atom.commands.add('atom-text-editor', {
            'drop-hungarian:drop': () => this.drop()
        }));
    },

    deactivate() {
        this.modalPanel.destroy();
        this.subscriptions.dispose();
        this.dropHungarianView.destroy();
    },

    serialize() {
        return {
            dropHungarianViewState: this.dropHungarianView.serialize()
        };
    },

    toggle() {
        console.log('DropHungarian was toggled!');
        return (
            this.modalPanel.isVisible() ?
            this.modalPanel.hide() :
            this.modalPanel.show()
        );
    },

    drop() {
        var editor = atom.workspace.getActiveTextEditor(),
            text = editor.getText(), lines;

        text = text.replace(/([\s\(\)\,\;\[\]\!\=\'\"\&\*\+\-\/\%\{\}\>\<]\$*\_*)([absmnof])([A-Z][a-zA-Z]+)/g, (match, p1, p2, p3) => p1 + p3.substring(0, 1).toLowerCase() + p3.substring(1));
        editor.setText(text);
    },

};
